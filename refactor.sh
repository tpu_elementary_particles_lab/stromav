#!/bin/sh

SOURCE_FILES_ENCODED=`                          \
    find . \( -name "*.hh"   -o -name "*.cc"    \
           -o -name "*.h"    -o -name "*.c"     \
           -o -name "*.hpp"  -o -name "*.cpp"   \
           -o -name "*.ihpp" -o -name "*.icpp"  \
           \) -type f -print0 | xxd -p`

# produces hex dump
#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | od -c

# Prints files to be refactored:
#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
#    echo -e "{}"

#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
#    grep ::aframe "{}" -n --color

#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
#    grep ::aframe "{}" -n --color

find . -name "*aframe*" -type f -print

