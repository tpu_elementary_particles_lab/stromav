/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_AFRAME_CONFIG_H
# define H_AFRAME_CONFIG_H

# define AFRAME_VERSION_MAJOR 0
# define AFRAME_VERSION_MINOR 1

/* Where the common config file is supposed to be found.
 * (in most cases, this can be overriden by providing -c
 * option to application)
 */
# define AFRAME_DEFAULT_CONFIG_FILE_PATH "/home/crank/Projects/CERN/na64-meta/install/etc/AFrame/defaults.cfg"

/* Features
 */
#define GEANT4_MC_MODEL            1
#define G4_MDL_VIS                 2
#define G4_MDL_GUI                 3
#define RPC_PROTOCOLS              4
#define ANALYSIS_ROUTINES          5
#define GEANT4_DYNAMIC_PHYSICS     6
#define ALIGNMENT_ROUTINES         7

/* Some third-party libraries require this macro to be set.
 */
# ifndef PACKAGE
#   define PACKAGE aframe
#   define PACKAGE_VERSION AFRAME_VERSION_MAJOR AFRAME_VERSION_MINOR
# endif

/* Diagnostic aux info
 */
# define AFRAME_BUILD_TIMESTAMP        "30.11.2016 30.11.2016"
# define AFRAME_BUILD_C_FLAGS             -Wall -D_GNU_SOURCE -fexceptions -pthread -D_FILE_OFFSET_BITS=64 -DHAVE_INLINE -std=c99 -rdynamic
# define AFRAME_BUILD_CXX_FLAGS        -march=k8 -O2 -pipe -W -Wall -pedantic -Wno-non-virtual-dtor -Wno-long-long -Wwrite-strings -Wpointer-arith -Woverloaded-virtual -Wno-variadic-macros -Wshadow -pipe -std=c++98 -Wall -D_GNU_SOURCE -fexceptions -pthread -D_FILE_OFFSET_BITS=64 -std=gnu++11 -Wno-c99-extensions -rdynamic
# define AFRAME_CXX_COMPILER           /usr/bin/c++
# define AFRAME_C_COMPILER             /usr/bin/cc
# define AFRAME_CMAKE_INSTALL_PREFIX   /home/crank/Projects/CERN/na64-meta/install
# define AFRAME_BUILDER_HOSTNAME       fresnel
# define AFRAME_CMAKE_SYSTEM           Linux-4.4.26-gentoo
# define AFRAME_BUILD_TYPE             RelWithDebInfo

# ifndef NDEBUG
#   define safe_cast dynamic_cast
# else
#   define safe_cast static_cast
# endif

# endif  /* H_AFRAME_CONFIG_H */

